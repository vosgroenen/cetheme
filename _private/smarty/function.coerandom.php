<?php


/**
 * @param $params [min, max]
 * @param $smarty
 * @return int
 */
function smarty_function_coerandom($params, &$smarty) {
    $min = (int)$params['min'];
    $max = (int)$params['max'];
    srand((double)microtime() * 1000000);
    return rand($min, $max);
} 