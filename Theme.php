<?php

namespace Shopware\Themes\LoebkeUebung;

use Shopware\Components\Form as Form;

class Theme extends \Shopware\Components\Theme
{
    protected $extend = 'Responsive';

    protected $name = <<<'SHOPWARE_EOD'
Theme für Loebke
SHOPWARE_EOD;

    protected $injectBeforePlugins =  true;

    protected $description = <<<'SHOPWARE_EOD'

SHOPWARE_EOD;

    protected $author = <<<'SHOPWARE_EOD'
nv
SHOPWARE_EOD;

    protected $license = <<<'SHOPWARE_EOD'

SHOPWARE_EOD;

    protected $javascript = [
        'src/js/custom.js'
    ];
    public function createConfig(Form\Container\TabContainer $container)
    {
    }
}