{extends file="parent:frontend/detail/content.tpl"}

{block name='frontend_index_content_inner'}
    {*<picture class="wrapperImageDetailDesktop">*}
    {*<img class="imageDetailDesktop" src="{$sArticle.image.thumbnails.0.source}">*}
    {*</picture>*}
    <div class="detailWrapper" itemscope
         itemtype="http://schema.org/Product"{if !{config name=disableArticleNavigation}} data-product-navigation="{url module="widgets" controller="listing" action="productNavigation"}" data-category-id="{$sArticle.categoryID}" data-main-ordernumber="{$sArticle.mainVariantNumber}"{/if}
         data-ajax-wishlist="true"
         data-compare-ajax="true"{if $theme.ajaxVariantSwitch} data-ajax-variants-container="true"{/if}>
        <h2 class="headlineDetailSM">
            {$sArticle.articleName}
        </h2>
        {block name='frontend_detail_index_configurator_settings'}
            {* Variable for tracking active user variant selection *}
            {$activeConfiguratorSelection = true}
            {if $sArticle.sConfigurator && ($sArticle.sConfiguratorSettings.type == 1 || $sArticle.sConfiguratorSettings.type == 2)}
                {* If user has no selection in this group set it to false *}
                {foreach $sArticle.sConfigurator as $configuratorGroup}
                    {if !$configuratorGroup.selected_value}
                        {$activeConfiguratorSelection = false}
                    {/if}
                {/foreach}
            {/if}
        {/block}
        {* Product image *}
        {block name='frontend_detail_index_image_container'}
            <div class="block-group coe-product--detail-upper-image mobileImage">
                <div class="product--image-container image-slider{if $sArticle.image && {config name=sUSEZOOMPLUS}} product--image-zoom{/if}"
                        {if $sArticle.image}
                    data-image-slider="true"
                    data-image-gallery="true"
                    data-maxZoom="{$theme.lightboxZoomFactor}"
                    data-thumbnails=".image--thumbnails"
                        {/if}>
                    {include file="frontend/detail/image.tpl"}
                </div>
            </div>
        {/block}
        <div class="wrapperProductInfo">
            <h2 class="headlineDetail">
                {$sArticle.articleName}
            </h2>
            <div class="infoDetail">
                <div class="wrapperDetailInfo">
                    <div class="detailPositions">
                        {if $sArticle.ordernumber}
                            <p>Artikel-Nr.:</p>
                        {/if}
                        {if $sArticle.shippingtime}
                            <p>Lieferzeit:</p>
                        {/if}
                        {if $sArticle.referenceprice}
                            <p>Preis/kg:</p>
                        {/if}
                    </div>
                    <div class="detailValue">
                        {if $sArticle.ordernumber}
                            <p>{$sArticle.ordernumber}</p>
                        {/if}
                        {if $sArticle.shippingtime}
                            <p>{$sArticle.shippingtime} Tage</p>
                        {/if}
                        {if $sArticle.referenceprice}
                            <p>{$sArticle.referenceprice}€ /100g</p>
                        {/if}
                    </div>
                </div>
                <div class="product--detail-upper">
                    <div class="productVariant" id="variantVisibilty"
                         {if $sArticle.sConfigurator}data-has-variant="true"{/if}>
                        {*Configurator drop down menus *}
                        {block name="frontend_detail_index_configurator"}
                            <div class="product--configurator">
                                {if $sArticle.sConfigurator}
                                    {if $sArticle.sConfiguratorSettings.type == 1}
                                        {$file = 'frontend/detail/config_step.tpl'}
                                    {elseif $sArticle.sConfiguratorSettings.type == 2}
                                        {$file = 'frontend/detail/config_variant.tpl'}
                                    {else}
                                        {$file = 'frontend/detail/config_upprice.tpl'}
                                    {/if}
                                    {include file=$file}
                                {/if}
                            </div>
                        {/block}
                    </div>
                    <div class="productBuy  block-group ">
                        {block name="frontend_detail_index_buybox"}
                            <p class="articelPrice">
                                {$sArticle.price}
                            </p>
                        {/block}
                        {include file="frontend/detail/buy.tpl"}
                            {include file="frontend/detail/actions.tpl"}
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="descriptionDetail">
        {block name="frontend_detail_index_tabs"}
            {include file="frontend/detail/tabs.tpl"}
        {/block}
    </div>
{/block}