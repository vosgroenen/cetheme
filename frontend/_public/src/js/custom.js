/**Variant Hide JS**/

$(function () {
    var variantField = $('[data-has-variant="true"]');
    if (variantField[0]) {
        $("#variantVisibilty").removeClass("is--hidden");
    } else {
        $("#variantVisibilty").addClass("is--hidden");
    }
});