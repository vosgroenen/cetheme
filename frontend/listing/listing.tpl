{extends file="parent:frontend/listing/listing.tpl"}



{block name="frontend_listing_list_promotion"}
    {$smarty.block.parent}
    <div class="containerListingWrapper">

        {$isMainCategorie = false}
        {foreach from=$sCategories item=sCategorie}
            {if $sCategoryContent.parentId === $Shop->getCategory()->getId()}
                {$isMainCategorie = true}
            {else}
                {$isMainCategorie = false}
            {/if}

        {/foreach}
        {if $isMainCategorie == true}
            {foreach from=$sCategories item=sCategorie}
                {foreach from=$sCategorie.subcategories  item=subcategorie}
                    <div class="containerListingBox" style="transform:rotate({coerandom min=-3 max=3}deg)">
                        <div class="wrapperListingShadow">
                            {if $subcategorie.media.path}
                                {$categoryImage = $subcategorie.media.path}
                            {else}
                                {$categoryImage = {link file='frontend/_public/src/img/noImage.png'}}
                            {/if}
                            <div class="backgroundPicture" href="{$subcategorie.link}">
                                <a href="{$subcategorie.link}">
                                    <div class="wrapperListingPicture">
                                        <div class="listingPicture"
                                             style="background-image:url('{$subcategorie.media.path}');" alt="Picture">
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <div class="subCategorieName">
                            <a href="{$subcategorie.link}">
                                {$subcategorie.name}
                            </a>
                        </div>
                    </div>
                {/foreach}
            {/foreach}
        {/if}
    </div>
{/block}
