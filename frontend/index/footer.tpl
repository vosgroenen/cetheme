{extends file="parent:frontend/index/footer.tpl"}
{block name="frontend_index_shopware_footer"}

    {* Copyright *}
    {block name="frontend_index_shopware_footer_copyright"}
        <div class="footer--copyright">
            {s name="coeIndexCopyright" namespace="frontend/index/footerCopyright"}Copyright © codeenterprise GmbH - Mindener Straße 330 - 49086 Osnabrück {/s}
        </div>
    {/block}

    {* Logo *}

{/block}