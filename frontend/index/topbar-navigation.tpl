{extends file="parent:frontend/index/topbar-navigation.tpl"}
{block name="frontend_index_top_bar_main"}

    <div class="top-bar">
        <div class="container top-bar-container block-group">

            {*Top bar navigation*}
            {block name="frontend_index_top_bar_nav"}
                <div class="information--topbar-wrapper">
                    <ul>
                        <li class="topbarLieferung">
                        <span>
                        {s name="topbarVersandImage" namespace="frontend/index/topbar-navigation"}<img class="iconTopbar" src="{link file='frontend/_public/src/img/icons/iconVersand.png'}">{/s}
                            {s name="topbarVersand" namespace="frontend/index/topbar-navigation"}Schneller Versand{/s}
                        </span>
                        </li>
                        <li class="topbarService">
                        <span>
                        {s name="topbarLieferungImage" namespace="frontend/index/topbar-navigation"}<img class="iconTopbar" src="{link file='frontend/_public/src/img/icons/iconLieferung.png'}">{/s}
                            {s name="topbarLieferung" namespace="frontend/index/topbar-navigation"}Kostenlose Lieferung ab 40 €{/s}
                        </span>
                        </li>
                        <li class="topbarVersand">
                        <span>
                          {s name="topbarHotlineImage" namespace="frontend/index/topbar-navigation"}  <img class="iconTopbar" src="{link file='frontend/_public/src/img/icons/iconTelefon.png'}">{/s}
                            {s name="topbarHotline" namespace="frontend/index/topbar-navigation"}Service Hotline: 0541 - 3 80 33-94{/s}
                        </span>
                        </li>
                    </ul>
                </div>
            {/block}
        </div>
    </div>

        <div class="top-bar2">

            {* Top bar main container *}
            {block name="frontend_index_top_bar_main_container"}
                <div class="container block-group">

                    {* Top bar navigation *}
                    {block name="frontend_index_top_bar_nav"}
                        <nav class="top-bar--navigation block" role="menubar">

                            {action module=widgets controller=index action=shopMenu}

                            {* Article Compare *}
                            {block name='frontend_index_navigation_inline'}
                                {if {config name="compareShow"}}
                                    <div class="navigation--entry entry--compare is--hidden" role="menuitem" aria-haspopup="true" data-drop-down-menu="true">
                                        {block name='frontend_index_navigation_compare'}
                                            {action module=widgets controller=compare}
                                        {/block}
                                    </div>
                                {/if}
                            {/block}

                        </nav>
                    {/block}
                </div>
            {/block}
        </div>

    {*{if $smarty.get.debug eq 1}*}

{/block}