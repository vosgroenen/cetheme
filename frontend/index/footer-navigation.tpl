{extends file="parent:frontend/index/footer-navigation.tpl"}
{namespace name="frontend/index/menu_footer"}

{* Service hotline *}
{block name="frontend_index_footer_column_service_hotline"}
    <div class="footer--column column--newsletter block">
        {block name="frontend_index_footer_column_newsletter_headline"}
            <div class="column--headline headlineNewsletter">{s name="sFooterNewsletterHead"}{/s}</div>
            {*<img class="column--headline imageNewsletter" src="{link file='frontend/_public/src/img/icons/newsletterIcon.png'}"/>*}
            <svg id="Capa_1" class="imageNewsletter" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496 496"><title>
                    newsletter234</title>
                <path d="M368,64V80H475.06l-12.94,25.89A39.79,39.79,0,0,1,426.34,128H368v16h58.34a55.71,55.71,0,0,0,50.09-31l3.58-7.16V454.1l-3.58-7.16A55.71,55.71,0,0,0,426.34,416H181.66a55.71,55.71,0,0,0-50.09,31L128,454.11V384H112V496H496V64ZM132.94,480l12.94-25.89A39.79,39.79,0,0,1,181.66,432H426.34a39.79,39.79,0,0,1,35.78,22.11L475.06,480Z"/>
                <path d="M352,0H0V368H352ZM336,352H16V16H336Z"/>
                <path d="M176,32H32V176H176ZM160,160H48V48H160Z"/>
                <rect x="192" y="32" width="128" height="16"/>
                <rect x="224" y="64" width="96" height="16"/>
                <rect x="192" y="96" width="128" height="16"/>
                <rect x="192" y="128" width="128" height="16"/>
                <rect x="192" y="160" width="96" height="16"/>
                <rect x="64" y="192" width="96" height="16"/>
                <rect x="32" y="224" width="128" height="16"/>
                <rect x="32" y="256" width="128" height="16"/>
                <rect x="32" y="288" width="128" height="16"/>
                <rect x="144" y="320" width="16" height="16"/>
                <rect x="32" y="320" width="96" height="16"/>
                <path d="M248,192a72,72,0,1,0,72,72A72.08,72.08,0,0,0,248,192Zm0,128a56,56,0,1,1,56-56A56.07,56.07,0,0,1,248,320Z"/>
                <rect x="304" y="160" width="16" height="16"/>
                <rect x="192" y="64" width="16" height="16"/>
                <rect x="32" y="192" width="16" height="16"/>
            </svg>
        {/block}

        {block name="frontend_index_footer_column_newsletter_content"}
            <div class="column--content">
                <p class="column--desc">
                    {s name="sFooterNewsletter"}{/s}
                </p>

                {block name="frontend_index_footer_column_newsletter_form"}
                    <form class="newsletter--form" action="{url controller='newsletter'}" method="post">
                        <input type="hidden" value="1" name="subscribeToNewsletter"/>

                        {block name="frontend_index_footer_column_newsletter_form_field"}
                            <input type="email" name="newsletter" class="newsletter--field"
                                   placeholder="{s name="IndexFooterNewsletterValue"}{/s}"/>
                            {if {config name="newsletterCaptcha"} !== "nocaptcha"}
                                <input type="hidden" name="redirect">
                            {/if}
                        {/block}

                        {block name="frontend_index_footer_column_newsletter_form_submit"}
                            <button type="submit" class="newsletter--button btn">
                                <i class="icon--mail"></i> <span
                                        class="button--text">{s name='IndexFooterNewsletterSubmit'}{/s}</span>
                            </button>
                        {/block}
                    </form>
                {/block}
            </div>
        {/block}
    </div>
{/block}

{block name="frontend_index_footer_column_service_menu"}
    <div class="footer--column column--menu serviceMenu block">
        {block name="frontend_index_footer_column_service_menu_headline"}
            <div class="column--headline">{s name="sFooterShopNavi1"}{/s}</div>
        {/block}

        {block name="frontend_index_footer_column_service_menu_content"}
            <nav class="column--navigation column--content">
                <ul class="navigation--list" role="menu">
                    {block name="frontend_index_footer_column_service_menu_before"}{/block}
                    {foreach $sMenu.gBottom as $item}

                        {block name="frontend_index_footer_column_service_menu_entry"}
                            <li class="navigation--entry" role="menuitem">
                                <a class="navigation--link"
                                   href="{if $item.link}{$item.link}{else}{url controller='custom' sCustom=$item.id title=$item.description}{/if}"
                                   title="{$item.description|escape}"{if $item.target} target="{$item.target}"{/if}>
                                    {$item.description}
                                </a>

                                {* Sub categories *}
                                {if $item.childrenCount > 0}
                                    <ul class="navigation--list is--level1" role="menu">
                                        {foreach $item.subPages as $subItem}
                                            <li class="navigation--entry" role="menuitem">
                                                <a class="navigation--link"
                                                   href="{if $subItem.link}{$subItem.link}{else}{url controller='custom' sCustom=$subItem.id title=$subItem.description}{/if}"
                                                   title="{$subItem.description|escape}"{if $subItem.target} target="{$subItem.target}"{/if}>
                                                    {$subItem.description}
                                                </a>
                                            </li>
                                        {/foreach}
                                    </ul>
                                {/if}
                            </li>
                        {/block}
                    {/foreach}

                    {block name="frontend_index_footer_column_service_menu_after"}{/block}
                </ul>
            </nav>
        {/block}
    </div>
{/block}

{block name="frontend_index_footer_column_information_menu"}
    <div class="footer--column column--menu block">
        {block name="frontend_index_footer_column_information_menu_headline"}
            <div class="column--headline">{s name="sFooterShopNavi2"}{/s}</div>
        {/block}

        {block name="frontend_index_footer_column_information_menu_content"}
            <nav class="column--navigation column--content">
                <ul class="navigation--list" role="menu">
                    {block name="frontend_index_footer_column_information_menu_before"}{/block}
                    {foreach $sMenu.gBottom2 as $item}

                        {block name="frontend_index_footer_column_information_menu_entry"}
                            <li class="navigation--entry" role="menuitem">
                                <a class="navigation--link"
                                   href="{if $item.link}{$item.link}{else}{url controller='custom' sCustom=$item.id title=$item.description}{/if}"
                                   title="{$item.description|escape}"{if $item.target} target="{$item.target}"{/if}>
                                    {$item.description}
                                </a>

                                {* Sub categories *}
                                {if $item.childrenCount > 0}
                                    <ul class="navigation--list is--level1" role="menu">
                                        {foreach $item.subPages as $subItem}
                                            <li class="navigation--entry" role="menuitem">
                                                <a class="navigation--link"
                                                   href="{if $subItem.link}{$subItem.link}{else}{url controller='custom' sCustom=$subItem.id title=$subItem.description}{/if}"
                                                   title="{$subItem.description|escape}"{if $subItem.target} target="{$subItem.target}"{/if}>
                                                    {$subItem.description}
                                                </a>
                                            </li>
                                        {/foreach}
                                    </ul>
                                {/if}
                            </li>
                        {/block}
                    {/foreach}
                    {block name="frontend_index_footer_column_information_menu_after"}{/block}
                </ul>
            </nav>
        {/block}
    </div>
{/block}

{block name="frontend_index_footer_column_newsletter"}
{/block}
