{extends file="parent:frontend/index/index.tpl"}

{block name='frontend_index_navigation_categories_top'}
    {$smarty.block.parent}


    {if $Controller|lower == 'register'}
        <div class="headerBackground"></div>
    {/if}


    {if $Controller|lower == 'index'}
        <div class="headerBackground"></div>
    {/if}



{/block}

{block name='frontend_index_left_last_articles'}
    {if $sLastArticlesShow && !$isEmotionLandingPage}
        {* Last seen products *}
        <div class="last-seen-products is--hidden" data-last-seen-products="true">
            <div class="last-seen-products--title">
                {s namespace="frontend/plugins/index/viewlast" name='WidgetsRecentlyViewedHeadline'}{/s}
            </div>
            <div class="last-seen-products--slider product-slider" data-product-slider="true">
                <div class="last-seen-products--container product-slider--container" ></div>
            </div>
        </div>
    {/if}
{/block}